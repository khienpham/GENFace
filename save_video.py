from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

import cv2
import sys

import time
count_vid_file = 'count_vid.txt'
count_vid = 0
with open(count_vid_file) as f:
    for line in f:
        count_vid = int(line.strip())
        break
    f.close()
def process_video_margin(in_video, out_folder, idx_frame):
    video_path = 0# 'rtsp://admin:123456a@@10.208.209.63/channel1'
    video_in = cv2.VideoCapture(video_path)
    video_in.set(3, 1280)
    video_in.set(4, 720)
    save_path = out_folder
    # if not os.path.isdir(save_path):
    #     os.makedirs(save_path)
    if(video_in.isOpened() == False):
        print ('can not load video')
    count_frame = int(idx_frame)
    count = 0
    x =500
    y = 200
    x2 = 850
    y2 = 650

    fps = video_in.get(cv2.CAP_PROP_FPS)
    print(fps)
    video_file = 'outvideo.avi'
    video_save = str(count_vid) + video_file
    width = video_in.get(3)  # float
    height = video_in.get(4)
    outVid = cv2.VideoWriter(video_save, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), int(fps), (int(width), int(height)))
    while video_in.isOpened():
        ret, frame = video_in.read()
        if ret is False:
            print ('frame is empty ={}'.format(count_frame))
            sys.exit(0)
        frame1 = frame.copy()

        height, width = frame.shape[:2]

        cv2.rectangle(frame1, (x, y), (x2, y2), (0, 255, 0), 3)

        cv2.imshow('Video', frame1)
        cv2.waitKey(1)
        outVid.write(frame)
    video_in.release()
    outVid.release()
if __name__=='__main__':
    if len(sys.argv) < 4:
        print('need load source video, target folder, index frame')
        sys.exit(0)
    try:
        process_video_margin(sys.argv[1], sys.argv[2], sys.argv[3])
    except KeyboardInterrupt:
        count_vid += 1
        file = open(count_vid_file, 'w')
        file.write(str(count_vid))
        file.close()
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)