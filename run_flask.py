#!/usr/bin/env python
from importlib import import_module
import os
from flask import Flask, render_template, Response, request, jsonify
from webapp.camera_rtmp import Camera
from webapp.tasks import download, create_model_task

from redis import Redis
from rq import Queue

q = Queue(connection=Redis())

app = Flask(__name__)


@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/upload_videos', methods=['POST'])
def upload_videos():
    body = request.get_json()

    # process
    job = q.enqueue(download, args=(body,), timeout=500000)
    return jsonify(request.get_json())

@app.route('/create_model', methods=['POST'])
def create_model():
    body = request.get_json()

    # process
    job = q.enqueue(create_model_task, args=(body,), timeout=500000)
    return jsonify(request.get_json())

@app.route('/convert', methods=['POST'])
def convert():
    # how to store logging
    # we will add an output file in the folder
    # it will record each step
    # the file will be transfer to face2face-demo too
    # we will have an API to update the log

    # show the user profile for that user
    body = request.get_json()
    print("==== convert")
    print(body)
    print("============")
    
    filename = body["filename"]
    video_id = body["video_id"]
    model_id = body["model_id"]

    job = q.enqueue(convertF2f, filename, video_id, model_id)
    return '%s processed' % filename

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, threaded=True)
