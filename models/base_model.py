import os
import torch


class BaseModel():
    def name(self):
        return 'BaseModel'

    def initialize(self, opt):
        self.opt = opt
        self.gpu_ids = opt.gpu_ids
        self.isTrain = opt.isTrain
        self.Tensor = torch.cuda.FloatTensor if self.gpu_ids else torch.Tensor
        self.save_dir = os.path.join(opt.checkpoints_dir, opt.name)

    def set_input(self, input):
        self.input = input

    def forward(self):
        pass

    # used in test time, no backprop
    def test(self):
        pass

    def get_image_paths(self):
        pass

    def optimize_parameters(self):
        pass

    def get_current_visuals(self):
        return self.input

    def get_current_errors(self):
        return {}

    def save(self, label):
        pass

    # helper saving function that can be used by subclasses
    def save_network(self, network, network_label, epoch_label, gpu_ids):
        save_filename = '%s_net_%s.pth' % (epoch_label, network_label)
        save_path = os.path.join(self.save_dir, save_filename)
        torch.save(network.cpu().state_dict(), save_path)
        if len(gpu_ids) and torch.cuda.is_available():
            network.cuda(gpu_ids[0])

    # helper loading function that can be used by subclasses
    def load_network(self, network, network_label, epoch_label):
        save_filename = '%s_net_%s.pth' % (epoch_label, network_label)
        save_path = os.path.join(self.save_dir, save_filename)
        # add more
        new_params = torch.load(save_path)
 #       updated_params.pop('fc.weight', None)
 #       updated_params.pop('fc.bias', None)
 #       new_params = network.state_dict()
 #       new_params.update(updated_params)

        # begin fix
        if not self.isTrain:
            missings = ["model.10.conv_block.5.weight", "model.10.conv_block.5.bias",
                        "model.11.conv_block.5.weight", "model.11.conv_block.5.bias",
                        "model.12.conv_block.5.weight", "model.12.conv_block.5.bias",
                        "model.13.conv_block.5.weight", "model.13.conv_block.5.bias",
                        "model.14.conv_block.5.weight", "model.14.conv_block.5.bias",
                        "model.15.conv_block.5.weight", "model.15.conv_block.5.bias"]

            unexpecteds = ["model.10.conv_block.6.weight", "model.10.conv_block.6.bias",
                           "model.11.conv_block.6.weight", "model.11.conv_block.6.bias",
                           "model.12.conv_block.6.weight", "model.12.conv_block.6.bias",
                           "model.13.conv_block.6.weight", "model.13.conv_block.6.bias",
                           "model.14.conv_block.6.weight", "model.14.conv_block.6.bias",
                           "model.15.conv_block.6.weight", "model.15.conv_block.6.bias"]

            for i in range(len(missings)):
                new_params[missings[i]] = new_params.pop(unexpecteds[i])


        # begin fix
        # missings = ["model.10.conv_block.6.weight", "model.10.conv_block.6.bias",
        #             "model.11.conv_block.6.weight", "model.11.conv_block.6.bias",
        #             "model.12.conv_block.6.weight", "model.12.conv_block.6.bias",
        #             "model.13.conv_block.6.weight", "model.13.conv_block.6.bias",
        #             "model.14.conv_block.6.weight", "model.14.conv_block.6.bias",
        #             "model.15.conv_block.6.weight", "model.15.conv_block.6.bias"]
        #
        # unexpecteds = ["model.10.conv_block.5.weight", "model.10.conv_block.5.bias",
        #                "model.11.conv_block.5.weight", "model.11.conv_block.5.bias",
        #                "model.12.conv_block.5.weight", "model.12.conv_block.5.bias",
        #                "model.13.conv_block.5.weight", "model.13.conv_block.5.bias",
        #                "model.14.conv_block.5.weight", "model.14.conv_block.5.bias",
        #                "model.15.conv_block.5.weight", "model.15.conv_block.5.bias"]
        #
        # for i in range(len(missings)):
        #     new_params[missings[i]] = new_params.pop(unexpecteds[i])
        network.load_state_dict(new_params)

   #     updated_params = torch.load(save_path)
   #     updated_params.pop('fc.weight', None)
   #     updated_params.pop('fc.bias', None)
   #     new_params = network.state_dict()
   #     new_params.update(updated_params)
        #
 #       network.load_state_dict(torch.load(save_path))
   #     network.load_state_dict(state_dict)

    # update learning rate (called once every epoch)
    def update_learning_rate(self):
        for scheduler in self.schedulers:
            scheduler.step()
        lr = self.optimizers[0].param_groups[0]['lr']
        print('learning rate = %.7f' % lr)
