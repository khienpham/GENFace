import os
import tensorflow as tf
import numpy as np
import cv2
import sys

if len(sys.argv) <3:
    print ('need load source and target folder')
    sys.exit(0)
if __name__ == '__main__':
    source_path = sys.argv[1] #'/media/khien/64951b34-a177-4b5d-8733-c33fd53ac60c/DATA/Alt++/Code/Recycle_GAN/faces/JapanObama/Japan/Japan_1001'
    target_path = sys.argv[2] #'/media/khien/64951b34-a177-4b5d-8733-c33fd53ac60c/DATA/Alt++/Code/Recycle_GAN/faces/JapanObama/Japan/Japan_1001_combine3'
    try:
        if not os.path.isdir(target_path):
            os.makedirs(target_path)
        lst_file = os.listdir(source_path)
        lst_file = sorted(lst_file)

        for i in range(len(lst_file)-2):
                img_file1 = os.path.join(source_path, lst_file[i])
                img_file2 = os.path.join(source_path, lst_file[i+1])
                img_file3 = os.path.join(source_path, lst_file[i+2])

                img1 = cv2.imread(img_file1)
                img2 = cv2.imread(img_file2)
                img3 = cv2.imread(img_file3)

                # Combine
                if np.shape(img1) == () or np.shape(img2) == () or np.shape(img3) == ():
                    continue
                img_combine1 = np.concatenate((img1, img2), axis=1)
                img_combine2 = np.concatenate((img_combine1, img3), axis=1)
                save_file = os.path.join(target_path, str('{0:05}'.format(i))+ '.jpg')
                cv2.imwrite (save_file, img_combine2)
        print ('process finish !!!')
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)