from PIL import Image
from io import BytesIO, StringIO
import base64
import cv2
import numpy as np
import time
import os
import shutil

def pil_image_to_base64(pil_image):
    buf = BytesIO()
    pil_image.save(buf, format="JPEG")
    return base64.b64encode(buf.getvalue())


def base64_to_pil_image(base64_img):
    return Image.open(BytesIO(base64.b64decode(base64_img)))

def base64_to_opencv_image(base64_img):
    # sbuf = StringIO()
    # sbuf.write(base64.b64decode(base64_img))
    pimg = Image.open(BytesIO(base64.b64decode(base64_img)))
    return cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)

# def base64_to_opencv_image(base64_img):
#     nparr = np.fromstring(base64_img, np.uint8)
#     img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
#     return img

def makedirs(folder):
    if not os.path.exists(folder):
        os.makedirs(folder, exist_ok=True)

def get_timestamp():
    return int(round(time.time() * 1000))

def rmdirs(folder):
    shutil.rmtree(folder)

def rm_file(file):
    os.remove(file)

def rm_files_by_ext(folder, ext):
    test = os.listdir(folder)

    for item in test:
        if item.endswith(".%s" % ext):
            os.remove(os.path.join(folder, item))

def append_to_file(filename, text):
    with open(filename, "a") as file:
        file.write(text)