
from __future__ import division
from .base_camera import BaseCamera

import sys
from itertools import starmap

from PyQt4 import QtCore, QtGui, uic
import numpy as np
from face_detect import Detection
from options.test_options import TestOptions
from data.data_loader import CreateDataLoader
from models.models import create_model
from util.visualizer import Visualizer
from util import html
import cv2
import torchvision.transforms as transforms
import torch
import time
opt = TestOptions().parse()
opt.nThreads = 1   # test code only supports nThreads = 1
opt.batchSize = 1  # test code only supports batchSize = 1
opt.serial_batches = True  # no shuffle
opt.no_flip = True  # no flip

data_loader = CreateDataLoader(opt)
dataset = data_loader.load_data()
model = create_model(opt)
# visualizer = Visualizer(opt)
test_transform = transforms.Compose([
    transforms.Scale((256,256)),  # resized to the network's required input size
    transforms.ToTensor(),])

img_size = 256
img_display = 480
cap = None
outVideo = None
video_target = 'outVid.avi'
fps = 25
x = 0
y =0
x2 = 0
y2 = 0

x_dis = 0
y_dis = 0
x2_dis = 0
y2_dis = 0

isCapture = False
isSaveVideo = False
isCamMode = 0 # 0:crop  1:full  2:camera
isAtoB = True
videoFileName = ''
isDisplayFull = False

# ------
x_dis = 500
y_dis = 200
x2_dis = 850
y2_dis = 650

x = 550
y = 250
x2 = 800
y2 = 600

def img_process(frame):
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    imgA_src = cv2.resize(frame, (img_size, img_size))
    imgA = np.asarray(imgA_src, dtype=np.float32)
    imgA = imgA / 255.
    input_A = np.transpose(imgA, (2, 0, 1))
    input_A = np.expand_dims(input_A, 0).copy()
    input_A = torch.from_numpy(input_A)
    if isAtoB == False:
        fake = model.testVid_genA(input_A)
    elif isAtoB == True:
        fake = model.testVid_genB(input_A)
    # Combined image
    img_combined = np.concatenate((imgA_src, fake), axis=1)
    img_combined = cv2.resize(img_combined, (2*img_display, img_display))
    return img_combined

class Camera(BaseCamera):
    video_source = "rtmp://34.80.59.81/app/live"

    @staticmethod
    def set_video_source(source):
        Camera.video_source = source

    @staticmethod
    def frames():
        camera = cv2.VideoCapture(Camera.video_source)
        if not camera.isOpened():
            raise RuntimeError('Could not start camera.')

        while True:
            # read current frame
            _, img = camera.read()
            img_combined = img_process(img)
            # img_combined = img_process(img[y:y2, x:x2])
            img_save = cv2.cvtColor(img_combined, cv2.COLOR_RGB2BGR)

            # encode as a jpeg image and return it
            yield cv2.imencode('.jpg', img_save)[1].tobytes()
