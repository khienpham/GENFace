import os
import requests
from .utils import *
import urllib.request

from dotenv import load_dotenv
load_dotenv(verbose=True)

import time

from options.train_options import TrainOptions
from data.data_loader import CreateDataLoader
from models.models import create_model
from util.visualizer import Visualizer

project_folder=os.getcwd()

def download(body):
    user_id = body["user_id"]

    # folder to store user data
    user_folder = "%s/download/videos/%s" % (project_folder, user_id)
    makedirs(user_folder)

    # user folder to store final image
    user_image_folder = "%s/images" % (user_folder)
    makedirs(user_image_folder)

    # output folder for this download time
    output_folder = "%s/videos_%d" % (user_folder, int(time.time()))
    makedirs(output_folder)

    # output file after merged
    output_file  = "%s/%s" % (output_folder, "merged.avi")

    # output folder in progress
    detected_folder = "%s/detected" % output_folder
    makedirs(detected_folder)

    combined_folder = "%s/combined" % output_folder
    makedirs(combined_folder)

    # log file in progress
    logfile = "%s/log.txt" % output_folder; 
    
    # download files
    append_to_file(logfile, "Downlaoding files.\n")
    input_command = ""
    matrix_command = ""
    index = 0
    for filename in body["filenames"]:
        url = "%s/%s" % ("https://kmsdo.socicom.xyz:9443/uploaded/chunks/", filename) # TODO: fix url
        urllib.request.urlretrieve(url, "%s/%s" % (output_folder, filename))

        input_command = input_command + "-i " + output_folder + "/" + filename + " "
        matrix_command = matrix_command + "[%d:v] [%d:a] " % (index, index)

        index = index + 1
    
    os.chdir(output_folder)
    commands = [
        [
            'ffmpeg -y %s -filter_complex "%s concat=n=%d:v=1:a=1 [v] [a]" -map "[v]" -map "[a]" %s' % (input_command, matrix_command, len(body["filenames"]), output_file),
            "Merging files"
        ],
        [
            "python3 %s/face_detect_4train.py %s %s 0 0" % (project_folder, output_file, detected_folder), 
            "Detecting faces"
        ],
        [
            "python3 %s/combine_images.py %s %s" % (project_folder, detected_folder, combined_folder), 
            "Combining images"
        ],
        [
            "mv %s/* %s/" % (combined_folder, user_image_folder), 
            "Moving files"
        ],
        
    ]

    for command in commands:
        append_to_file(logfile, command[1] + "\n")
        print("===============")
        print (command[0])
        print("===============")
        result = os.system(command[0])

        if result != 0:
            return

    # url = "https://kmsdo.socicom.xyz:9443/api/update_f2fmodel_status"  # TODO: fix url

    # json = {
    #     "f2fmodel_id": model_id,
    #     "status": "Finished"
    # }

    # append_to_file(logfile, "Changing face2face model generation status.\n")
    # response = requests.request("POST", url, json=json)

    # # clean up:
    # # delete: .mkv, .mov, combined, face2face-model, landmarks, landmarks_resized, original, original_resized
    # append_to_file(logfile, "Cleanning up data.\n")
    # folders = [
    #     'combined',
    #     'face2face-model',
    #     'landmarks',
    #     'landmarks_resized',
    #     'original',
    #     'original_resized',
    # ]

    # for f in folders:
    #     rmdirs("%s/%s" % (output_folder, f))
    
    # rm_files_by_ext(output_folder, "mkv");
    # rm_files_by_ext(output_folder, "mp4");

    # # sync folder
    # append_to_file(logfile, "Uploading data to Google cloud.\n")
    # os.system("gsutil rsync -dr /home/trungnt/alt.vm.f2f/download gs://nttrung143_alt/f2f")
    return

def create_model_task(body):
    model_id = body["model_id"]
    user_id_A = body["user_id_A"]
    user_id_B = body["user_id_B"]

    # folder to store user data
    model_folder = "%s/download/models/%s" % (project_folder, model_id)
    data_folder = "%s/data" % model_folder
    makedirs(data_folder)

    checkpoint_folder = "%s/checkpoints" % model_folder
    makedirs(checkpoint_folder)

    # 2 users folder
    user_A_folder = "%s/download/%s" % (project_folder, user_id_A)
    user_A_image_folder = "%s/images" % (user_A_folder)
    user_A_trainning_image_folder = "%s/data/"

    user_B_folder = "%s/download/%s" % (project_folder, user_id_B)
    user_B_image_folder = "%s/images" % (user_B_folder)

    # log file in progress
    logfile = "%s/log.txt" % model_folder; 

    commands = [
        [
            "cp -r %s %s/trainA" % (user_A_image_folder, data_folder),
            "Moving User A's images"
        ],
        [
            "cp -r %s %s/trainB" % (user_B_image_folder, data_folder),
            "Moving User B's images"
        ],
        [
            "cp %s %s" % ("/home/altvn/data/DATA/pretrain/*.pth", checkpoint_folder),
            "Copy pretrain"
        ],
        # [
        #     "python3 train.py --model recycle_gan --which_model_netG resnet_6blocks --dataset_mode unaligned_triplet --gpu 0 --dataroot %s --name %s --batchSize 1 --checkpoints_dir %s" % (data_folder, "checkpoints", model_folder),
        #     "Trainning"
        # ],
        # [
        #     "python3 train.py --model recycle_gan --which_model_netG resnet_6blocks --dataset_mode unaligned_triplet --gpu 0 --dataroot /home/altvn/GENFace/faces/AtoB --name AtoB --batchSize 1 --checkpoints_dir /home/altvn/GENFace/test/checkpoints",
        #     "Trainning"
        # ]
        
    ]

    for command in commands:
        append_to_file(logfile, command[1] + "\n")
        print("===============")
        print (command[0])
        print("===============")
        result = os.system(command[0])

        if result != 0:
            return

    train(data_folder, "checkpoints", model_folder)
    # train("/home/altvn/GENFace/faces/AtoB", "checkpoints", "/home/altvn/GENFace/test/checkpoints")

    return

def train(dataroot, name, checkpoints_dir):
    import sys
    sys.argv = ['run_flask.py', '--model', 'cycle_gan', '--which_model_netG', 'resnet_6blocks', '--dataset_mode', 'unaligned', '--no_dropout', '--gpu', '0', '--loadSize', '256']

    opt = TrainOptions().parse()

    opt.model = "recycle_gan"
    opt.which_model_netG = "resnet_6blocks"
    opt.dataset_mode = "unaligned_triplet"
    opt.gpu = 0
    opt.batchSize = 1
    opt.dataroot = dataroot
    opt.name = name
    opt.checkpoints_dir = checkpoints_dir

    data_loader = CreateDataLoader(opt)
    dataset = data_loader.load_data()
    dataset_size = len(data_loader)
    print('#training images = %d' % dataset_size)

    model = create_model(opt)
    visualizer = Visualizer(opt)
    total_steps = 0

    for epoch in range(opt.epoch_count, opt.niter + opt.niter_decay + 1):
        epoch_start_time = time.time()
        epoch_iter = 0

        for i, data in enumerate(dataset):
            iter_start_time = time.time()
            visualizer.reset()
            total_steps += opt.batchSize
            epoch_iter += opt.batchSize
            model.set_input(data)
            model.optimize_parameters()

            if total_steps % opt.display_freq == 0:
                save_result = total_steps % opt.update_html_freq == 0
                visualizer.display_current_results(model.get_current_visuals(), epoch, save_result)

            if total_steps % opt.print_freq == 0:
                errors = model.get_current_errors()
                t = (time.time() - iter_start_time) / opt.batchSize
                visualizer.print_current_errors(epoch, epoch_iter, errors, t)
                if opt.display_id > 0:
                    visualizer.plot_current_errors(epoch, float(epoch_iter)/dataset_size, opt, errors)

            if total_steps % opt.save_latest_freq == 0:
                print('saving the latest model (epoch %d, total_steps %d)' %
                        (epoch, total_steps))
                model.save('latest')

        if epoch % opt.save_epoch_freq == 0:
            print('saving the model at the end of epoch %d, iters %d' %
                    (epoch, total_steps))
            model.save('latest')
            model.save(epoch)

        print('End of epoch %d / %d \t Time Taken: %d sec' %
                (epoch, opt.niter + opt.niter_decay, time.time() - epoch_start_time))
        model.update_learning_rate()

def convertF2f(filename, video_id, model_id):
    # create output folder
    output_folder = "%s/download/%s" % (project_folder, filename)
    makedirs(output_folder)

    # create log file
    logfile = "%s/log.txt" % output_folder; 

    # download file
    append_to_file(logfile, "Downlaoding file.\n")
    url = "%s/%s" % ("https://kmsdo.socicom.xyz:9443/uploaded/f2f/", filename)
    urllib.request.urlretrieve(url, "%s/%s" % (output_folder, filename))
    
    model_foler = "%s/download/%s" % (project_folder, model_id)
    model_path = "%s/face2face-reduced-model/frozen_model.pb" % model_foler
    input_file = "%s/%s.mp4" % (output_folder, filename)

    os.chdir(output_folder)

    commands = [
        # convert to mp4
        ["ffmpeg -y -i %s/%s -f mp4 -vcodec libx264 -preset fast -profile:v main -acodec aac %s/%s.mp4" % (output_folder, filename, output_folder, filename), "Converting to Mp4"],
        
        # extract sounds
        ["ffmpeg -i %s %s/output.wav" % (input_file, output_folder), "Extracting sound"],
        
        # convert into images
        ["python %s/run_video.py --source %s --show 0 --landmark-model %s/shape_predictor_68_face_landmarks.dat --tf-model %s --output-folder %s/images --log-file %s/log.txt" % (ff_folder, input_file, ff_folder, model_path, output_folder, output_folder), "Running face2face conversion"],

        # merge into video
        ["ffmpeg -y -framerate 24 -i %s/images/scene_%%06d.png -pix_fmt yuv420p %s/output_without_audio.mp4" % (output_folder, output_folder), "Merging images into video"],

        # merge sounds
        ["ffmpeg -y -i %s/output_without_audio.mp4 -i %s/output.wav %s/output.mp4" % (output_folder, output_folder, output_folder), "Merging sound"],
    ]

    for command in commands:
        append_to_file(logfile, command[1] + "\n")
        # print("===============")
        # print (command)
        # print("===============")
        result = os.system(command[0])

        if result != 0:
            return

    url = "https://kmsdo.socicom.xyz:9443/api/update_f2fvideo_status"

    json = {
        "f2fvideo_id": video_id,
        "status": "Finished"
    }
    append_to_file(logfile, "Changing video conversion status.\n")

    response = requests.request("POST", url, json=json)

    # clean up:
    append_to_file(logfile, "Cleanning up data.\n")
    rmdirs("%s/images" % (output_folder))
    
    rm_file("%s/%s" % (output_folder, filename));
    rm_file("%s/%s.mp4" % (output_folder, filename));
    rm_file("%s/output_without_audio.mp4" % (output_folder));
    rm_file("%s/output.wav" % (output_folder));

    # sync folder
    append_to_file(logfile, "Uploading data to Google cloud.\n")
    os.system("gsutil rsync -dr /home/trungnt/alt.vm.f2f/download gs://nttrung143_alt/f2f")
    append_to_file(logfile, "")
    return

def execute_command(command):
    return os.system(command)