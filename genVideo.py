import cv2
import numpy as np
import os

def process_video(img_src_path, img_tar_fake_path, img_size, fps, saveVideoFile):
    lst_img_src = os.listdir(img_src_path)
    lst_img_tar = os.listdir(img_tar_fake_path)
    lst_img_src = sorted(lst_img_src)
    lst_img_tar = sorted(lst_img_tar)

    outVid = cv2.VideoWriter(saveVideoFile, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), fps, (2*img_size, img_size))
    for i in range(len(lst_img_src)):
        img_src_file = os.path.join(img_src_path, lst_img_src[i])
        img_tar_file = os.path.join(img_tar_fake_path, lst_img_tar[i])
        img_src = cv2.imread(img_src_file)
        img_tar = cv2.imread(img_tar_file)
        # concenate two image
        img_combined = np.concatenate((img_src, img_tar), axis=1)
        img_combined = cv2.resize(img_combined, (2*img_size, img_size))
        cv2.imshow('test', img_combined)
        cv2.waitKey(1)
        outVid.write(img_combined)
    outVid.release()
def filter_folder(img_path, img_src_path, img_src_fake_path, img_tar_path, img_tar_fake_path):
    for file_name in os.listdir(img_path):
        file_path = os.path.join(img_path, file_name)
        img = cv2.imread(file_path)

        if file_name.find('fake_A') != -1:
            save_file = os.path.join(img_src_fake_path, file_name)
        elif file_name.find('fake_B') != -1:
            save_file = os.path.join(img_tar_fake_path, file_name)
        elif file_name.find('real_A') != -1:
            save_file = os.path.join(img_src_path, file_name)
        elif file_name.find('real_B') != -1:
            save_file = os.path.join(img_tar_path, file_name)
        else:
            continue

        cv2.imwrite(save_file, img)
import shutil
if __name__ == '__main__':
    img_path = './results/TuckerObamaTest2/test_latest/images'
    img_src_path = 'img_src'
    img_src_fake_path ='img_src_fake'
    img_tar_path = 'img_tar'
    img_tar_fake_path ='img_tar_fake'

    if os.path.isdir(img_src_path):
        shutil.rmtree(img_src_path)
    if os.path.isdir(img_src_fake_path):
        shutil.rmtree(img_src_fake_path)
    if os.path.isdir(img_tar_path):
        shutil.rmtree(img_tar_path)
    if os.path.isdir(img_tar_fake_path):
        shutil.rmtree(img_tar_fake_path)

    if not os.path.isdir(img_src_path):
        os.makedirs(img_src_path)
    if not os.path.isdir(img_src_fake_path):
        os.makedirs(img_src_fake_path)
    if not os.path.isdir(img_tar_path):
        os.makedirs(img_tar_path)
    if not os.path.isdir(img_tar_fake_path):
        os.makedirs(img_tar_fake_path)
    img_size = 256
    fps = 15
    # Process function
    filter_folder(img_path, img_src_path, img_src_fake_path, img_tar_path, img_tar_fake_path)
    process_video(img_tar_path, img_src_fake_path, img_size, fps, 'BtoA_41.avi')
    process_video(img_src_path, img_tar_fake_path, img_size, fps, 'AtoB_41.avi')
