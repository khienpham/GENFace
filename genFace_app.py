import time
import os
from options.test_options import TestOptions
from data.data_loader import CreateDataLoader
from models.models import create_model
from util.visualizer import Visualizer
from util import html
import cv2
import torchvision.transforms as transforms
import numpy as np
import torch
from keras import backend as K
import sys

opt = TestOptions().parse()
opt.nThreads = 1   # test code only supports nThreads = 1
opt.batchSize = 1  # test code only supports batchSize = 1
opt.serial_batches = True  # no shuffle
opt.no_flip = True  # no flip

data_loader = CreateDataLoader(opt)
dataset = data_loader.load_data()
model = create_model(opt)
visualizer = Visualizer(opt)
test_transform = transforms.Compose([
    transforms.Scale((256,256)),  # resized to the network's required input size
    transforms.ToTensor(),])

lstConf = []
with open(opt.config_file) as f:
    for line in f:
        lstConf.append(line.strip())
        print(line.strip())
if len(lstConf) <5:
    print('need check config file')
    exit(1)

video_source = lstConf[0].split('\t')[0]#"localhost"
video_target = lstConf[1].split('\t')[0]
whichAtoB = lstConf[2].split('\t')[0]
isDisplay = int(lstConf[3].split('\t')[0])
isSave = int(lstConf[4].split('\t')[0])

if __name__=='__main__':
    #img_path_vid = '../testVidJ2O/P1out_0601.avi'
    #img_path_vid = '../testVidJ2O/Out_Obama2_1_1.avi'
    vidA = cv2.VideoCapture(video_source)
    # vidB = cv2.VideoCapture(img_path_vidB)
    AtoB = int(whichAtoB)
    img_size = 256
    fps = 15
    outVid = cv2.VideoWriter(video_target, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), fps, (2 * img_size, img_size))
    try:
        while True:
            retA, imgA_src = vidA.read()

            if retA is False:
                print(' frame is empty')
                sys.exit()
                continue
            start = time.time()
            imgA_src = cv2.resize(imgA_src, (img_size, img_size))
            imgA = cv2.cvtColor(imgA_src, cv2.COLOR_BGR2RGB)

            imgA = np.asarray(imgA, dtype=np.float32)
            imgA = imgA/255.
            input_A = np.transpose(imgA, (2, 0, 1))
            input_A = np.expand_dims(input_A, 0).copy()
            input_A = torch.from_numpy(input_A)
            if AtoB == 0:
                fake = model.testVid_genA(input_A)
            elif AtoB == 1:
                fake = model.testVid_genB(input_A)
            # Combined image
            img_combined = np.concatenate((imgA_src, fake), axis=1)
            if isSave == 1:
                outVid.write(img_combined)
            if isDisplay == 1:
                cv2.imshow('display', img_combined)
                cv2.waitKey(1)
            print('time = {0:.3f}'.format(time.time() - start))
        outVid.release()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)