import time
import os
from options.test_options import TestOptions
from data.data_loader import CreateDataLoader
from models.models import create_model
from util.visualizer import Visualizer
from util import html
import cv2
import torchvision.transforms as transforms
import numpy as np
import torch
from keras import backend as K
import sys
import scipy.io as sio
from PyQt4 import QtGui, QtCore
import cv2
import numpy as np
from face_detect import Detection

opt = TestOptions().parse()
opt.nThreads = 1   # test code only supports nThreads = 1
opt.batchSize = 1  # test code only supports batchSize = 1
opt.serial_batches = True  # no shuffle
opt.no_flip = True  # no flip

data_loader = CreateDataLoader(opt)
dataset = data_loader.load_data()
model = create_model(opt)
# visualizer = Visualizer(opt)
test_transform = transforms.Compose([
    transforms.Scale((256,256)),  # resized to the network's required input size
    transforms.ToTensor(),])

# lstConf = []
# with open(opt.config_file) as f:
#     for line in f:
#         lstConf.append(line.strip())
#         print(line.strip())
# if len(lstConf) <5:
#     print('need check config file')
#     exit(1)
#
# video_source = lstConf[0].split('\t')[0]#"localhost"
# video_target = lstConf[1].split('\t')[0]
# whichAtoB = lstConf[2].split('\t')[0]
# isDisplay = int(lstConf[3].split('\t')[0])
# isSave = int(lstConf[4].split('\t')[0])

detector = Detection()

class VideoCapture(QtGui.QWidget):
    def __init__(self, filename, parent, selfall):
        super(QtGui.QWidget, self).__init__()
        self.cap = cv2.VideoCapture(str(filename))
        # self.video_frame = QtGui.QLabel('',parent)
        #parent.layout.addWidget(self.video_frame)

        self.video_frame = parent.video_frame1

        self.img_size = selfall.img_size
        self.AtoB = selfall.AtoB
        self.img_display = 480
        self.cam_mode = selfall.cam_mode
        self.x = 0
        self.y =0
        self.x2 = 0
        self.y2 = 0
        self.isSaveVideo = selfall.isSaveVideo

        self.video_target = 'outVid.avi'
        self.fps = 15
        self.outVid = cv2.VideoWriter(self.video_target, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), self.fps, (2 * self.img_display, self.img_display))


    def nextFrameSlot(self):
        ret, frame = self.cap.read()
        if ret is False:
            print ('can not load video')
            self.timer.stop()
        else:
            if self.cam_mode == 1:
                if self.x2 == 0 and self.y2 == 0:
                    self.x, self.y, self.x2, self.y2 = detector.process_face_detect(frame)
                    print ('x ={} y ={} x2={} y2={}'.format(self.x, self.y, self.x2, self.y2))

                frame = frame[self.y:self.y2, self.x:self.x2]

            # detect video and set rectangle
            img_combined = self.img_process(frame)

            img = QtGui.QImage(img_combined, 2*self.img_display, self.img_display, QtGui.QImage.Format_RGB888)
            pix = QtGui.QPixmap.fromImage(img)
            self.video_frame.setPixmap(pix)

            if self.isSaveVideo:
                img_save = cv2.cvtColor(img_combined, cv2.COLOR_RGB2BGR)
                self.outVid.write(img_save)
            if not self.isSaveVideo:
                self.outVid.release()

    def img_process(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        imgA_src = cv2.resize(frame, (self.img_size, self.img_size))
        imgA = np.asarray(imgA_src, dtype=np.float32)
        imgA = imgA / 255.
        input_A = np.transpose(imgA, (2, 0, 1))
        input_A = np.expand_dims(input_A, 0).copy()
        input_A = torch.from_numpy(input_A)
        if self.AtoB == False:
            fake = model.testVid_genA(input_A)
        elif self.AtoB == True:
            fake = model.testVid_genB(input_A)
        # Combined image
        img_combined = np.concatenate((imgA_src, fake), axis=1)
        img_combined = cv2.resize(img_combined, (2*self.img_display, self.img_display))
        return img_combined
    def start(self):
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.nextFrameSlot)
        self.timer.start(1000.0/25)

    def pause(self):
        self.timer.stop()

    def deleteLater(self):
        self.cap.release()
        super(QtGui.QWidget, self).deleteLater()


class VideoDisplayWidget(QtGui.QWidget):
    def __init__(self,parent):
        super(VideoDisplayWidget, self).__init__(parent)

        self.layout = QtGui.QFormLayout(self)

        self.startButton = QtGui.QPushButton('Start', parent)
        self.startButton.clicked.connect(parent.startCapture)
        self.startButton.setFixedWidth(80)
        self.startButton.setFixedHeight(40)
        self.pauseButton = QtGui.QPushButton('Pause', parent)
        self.pauseButton.setFixedWidth(80)
        self.pauseButton.setFixedHeight(40)

        self.checkBoxMode = QtGui.QCheckBox('AtoB', parent)
        self.checkBoxMode.move(10, 110)
        self.checkBoxMode.stateChanged.connect(parent.which_direction)

        self.checkBoxSaveVid = QtGui.QCheckBox('Save vid', parent)
        self.checkBoxSaveVid.move(10, 130)
        self.checkBoxSaveVid.stateChanged.connect(parent.save_video)

        self.comboBox = QtGui.QComboBox(parent)
        self.comboBox.addItem("CropVideo")
        self.comboBox.addItem("FullVideo")
        self.comboBox.addItem("Camera")
        self.comboBox.move(10, 80)
        self.comboBox.setFixedWidth(80)
        self.comboBox.activated[str].connect(parent.modecam_choice)

        self.exitButton = QtGui.QPushButton('Exit', parent)
        self.exitButton.setFixedWidth(80)
        self.exitButton.setFixedHeight(80)
        self.exitButton.move(10, 480)
        self.exitButton.clicked.connect(parent.closeApplication)

        self.video_frame1 = QtGui.QLabel('test', parent)
        self.video_frame1.move(100, 80)
        self.video_frame1.setFixedWidth(960)
        self.video_frame1.setFixedHeight(480)

        self.layout.addRow(self.startButton, self.pauseButton)

        self.setLayout(self.layout)


class ControlWindow(QtGui.QMainWindow):
    def __init__(self):
        super(ControlWindow, self).__init__()
        self.setGeometry(20, 20, 1070, 570)
        self.setWindowTitle("GEN Face")

        self.capture = None

        self.matPosFileName = None
        self.videoFileName = 0
        self.positionData = None
        self.updatedPositionData  = {'red_x':[], 'red_y':[], 'green_x':[], 'green_y': [], 'distance': []}
        self.updatedMatPosFileName = None

        self.isVideoFileLoaded = False

        self.quitAction = QtGui.QAction("&Exit", self)
        self.quitAction.setShortcut("Ctrl+Q")
        self.quitAction.setStatusTip('Close The App')
        self.quitAction.triggered.connect(self.closeApplication)

        self.openVideoFile = QtGui.QAction("&Open Video File", self)
        self.openVideoFile.setShortcut("Ctrl+Shift+V")
        self.openVideoFile.setStatusTip('Open .h264 File')
        self.openVideoFile.triggered.connect(self.loadVideoFile)

        self.mainMenu = self.menuBar()
        self.fileMenu = self.mainMenu.addMenu('&File')
        self.fileMenu.addAction(self.openVideoFile)
        self.fileMenu.addAction(self.quitAction)

        self.videoDisplayWidget = VideoDisplayWidget(self)
        self.setCentralWidget(self.videoDisplayWidget)

        self.AtoB = True
        self.cam_mode = 0       # 0:crop_video  1:full_video    2:camera
        self.isSaveVideo = False
        self.img_size = 256

    def startCapture(self):
        if not self.capture and self.isVideoFileLoaded:
            self.capture = VideoCapture(self.videoFileName, self.videoDisplayWidget, self)
            self.videoDisplayWidget.pauseButton.clicked.connect(self.capture.pause)
        self.capture.start()

    def endCapture(self):
        self.capture.deleteLater()
        self.capture = None

    def loadVideoFile(self):
        try:
            if self.capture:
                self.capture.deleteLater()
                self.capture = None
            self.videoFileName = QtGui.QFileDialog.getOpenFileName(self, 'Select .h264 Video File')
            self.isVideoFileLoaded = True
        except:
            print ("Please select a .h264 file")
    def which_direction(self, state):
        if state == QtCore.Qt.Checked:
            self.AtoB = False
            self.videoDisplayWidget.checkBoxMode.setText('BtoA')
        else:
            self.AtoB = True
            self.videoDisplayWidget.checkBoxMode.setText('AtoB')
        print(self.AtoB)
    def modecam_choice(self, text):
        # self.videoDisplayWidget.comboBox.setText(text)
        if text =='CropVideo':
            self.cam_mode = 0
        elif text == 'FullVideo':
            self.cam_mode = 1
        elif text == 'Camera':
            self.cam_mode = 2
        print(self.cam_mode)

    def save_video(self, state):
        if state == QtCore.Qt.Checked:
            self.isSaveVideo = True
        else:
            self.isSaveVideo = False
    def closeApplication(self):
        choice = QtGui.QMessageBox.question(self, 'Message','Do you really want to exit?',QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        if choice == QtGui.QMessageBox.Yes:
            print("Closing....")
            sys.exit()
        else:
            pass

if __name__=='__main__':

    app = QtGui.QApplication(sys.argv)
    window = ControlWindow()
    window.show()

    sys.exit(app.exec_())