from __future__ import division
import sys
from itertools import starmap
import cv2

from PyQt4 import QtCore, QtGui, uic
import numpy as np
from face_detect import Detection
from options.test_options import TestOptions
from data.data_loader import CreateDataLoader
from models.models import create_model
from util.visualizer import Visualizer
from util import html
import cv2
import torchvision.transforms as transforms
import torch
opt = TestOptions().parse()
opt.nThreads = 1   # test code only supports nThreads = 1
opt.batchSize = 1  # test code only supports batchSize = 1
opt.serial_batches = True  # no shuffle
opt.no_flip = True  # no flip

data_loader = CreateDataLoader(opt)
dataset = data_loader.load_data()
model = create_model(opt)
# visualizer = Visualizer(opt)
test_transform = transforms.Compose([
    transforms.Scale((256,256)),  # resized to the network's required input size
    transforms.ToTensor(),])

qtCreatorFile = "genFace.ui"  # Enter file here.

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.btnOpenFile.clicked.connect(self.loadVideoFile)
        self.btnStart.clicked.connect(self.start)
        self.btnPause.clicked.connect(self.pause)
        self.chkWhichDir.stateChanged.connect(self.which_direction)
        self.chkSaveVid.stateChanged.connect(self.save_video)
        self.cbModeCam.activated[str].connect(self.mode_cam_choice)
        self.btnExit.clicked.connect(self.Exit)

        self.detector = Detection()

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.main_process)
        self.img_size = 256
        self.img_display = 480
        self.cap = None
        self.outVideo = None
        self.video_target = 'outVid.avi'
        self.fps = 25
        self.x = 0
        self.y =0
        self.x2 = 0
        self.y2 = 0

        self.isCapture = False
        self.isSaveVideo = False
        self.isCamMode = 0 # 0:crop  1:full  2:camera
        self.isAtoB = True
        self.videoFileName = ''

    def CalculateTax(self):
        price = int(self.price_box.toPlainText())
        tax = (self.tax_rate.value())
        total_price = price + ((tax / 100) * price)
        labelText = "The total price with tax is: "
        total_price_string = str(total_price)
        self.results_window.setText(total_price_string)
        self.lblResult.setText(labelText)
    def loadVideoFile(self):
        try:
            if self.isCapture:
                self.pause()
                self.end_capture()
                self.x = 0
                self.y = 0
                self.x2 = 0
                self.y2 = 0
            self.videoFileName = QtGui.QFileDialog.getOpenFileName(self, 'Select Video File')
            self.cap = cv2.VideoCapture(str(self.videoFileName))
            self.isCapture = True
        except:
            print ("Please select a video file")
    def main_process(self):
        ret, frame = self.cap.read()
        if ret is False:
            print('can not load video')
            self.pause()
        else:
            if self.isCamMode == 1:
                if self.x2 == 0 and self.y2 == 0:
                    self.x, self.y, self.x2, self.y2 = self.detector.process_face_detect(frame)
                    print ('x ={} y ={} x2={} y2={}'.format(self.x, self.y, self.x2, self.y2))
            elif self.isCamMode == 2:
                self.x_dis = 500
                self.y_dis = 200
                self.x2 = 850
                self.y2 = 650

                frame = frame[self.y:self.y2, self.x:self.x2]

            # detect video and set rectangle
            img_combined = self.img_process(frame)

            self.display(img_combined)
            if self.isSaveVideo:
                img_save = cv2.cvtColor(img_combined, cv2.COLOR_RGB2BGR)
                self.outVideo.write(img_save)
    def display(self, img_combined):
        img = QtGui.QImage(img_combined, 2 * self.img_display, self.img_display, QtGui.QImage.Format_RGB888)
        pix = QtGui.QPixmap.fromImage(img)
        self.lblDisplay.setScaledContents(True)
        self.lblDisplay.setPixmap(pix)
    def img_process(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        imgA_src = cv2.resize(frame, (self.img_size, self.img_size))
        imgA = np.asarray(imgA_src, dtype=np.float32)
        imgA = imgA / 255.
        input_A = np.transpose(imgA, (2, 0, 1))
        input_A = np.expand_dims(input_A, 0).copy()
        input_A = torch.from_numpy(input_A)
        if self.isAtoB == False:
            fake = model.testVid_genA(input_A)
        elif self.isAtoB == True:
            fake = model.testVid_genB(input_A)
        # Combined image
        img_combined = np.concatenate((imgA_src, fake), axis=1)
        img_combined = cv2.resize(img_combined, (2*self.img_display, self.img_display))
        return img_combined
    def start(self):
        self.timer.start(40)
    def pause(self):
        self.timer.stop()
    def end_capture(self):
        self.cap.release()
    def mode_cam_choice(self, text):
        text = self.cbModeCam.currentText()
        if text == 'Crop Video':
            self.isCamMode = 0
        elif text == 'Full Video':
            self.isCamMode = 1
        elif text == 'Camera':
            self.isCamMode = 2
            if self.isCapture:
                self.pause()
                self.end_capture()
                self.x = 0
                self.y = 0
                self.x2 = 0
                self.y2 = 0
            self.cap = cv2.VideoCapture(0)
            # self.cap.set(3, 1280)
            # self.cap.set(4, 720)
            self.isCapture = True
        print(self.isCamMode)
    def which_direction(self):
        if self.chkWhichDir.isChecked():
            self.isAtoB = False
            self.chkWhichDir.setText('BtoA')
        else:
            self.isAtoB = True
            self.chkWhichDir.setText('AtoB')
        print(self.isAtoB)

    def save_video(self):
        if self.chkSaveVid.isChecked():
            self.isSaveVideo = True
            self.outVideo = cv2.VideoWriter(self.video_target, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), self.fps,
                                            (2 * self.img_display, self.img_display))

        else:
            self.isSaveVideo = False
            self.outVideo.release()
        print(self.isSaveVideo)

    def Exit(self):
        choice = QtGui.QMessageBox.question(self, 'Message','Do you really want to exit?',QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        if choice == QtGui.QMessageBox.Yes:
            print("Closing....")
            sys.exit()
        else:
            pass


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())