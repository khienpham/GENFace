import time
import os
from options.test_options import TestOptions
from data.data_loader import CreateDataLoader
from models.models import create_model
from util.visualizer import Visualizer
from util import html
import cv2
import torchvision.transforms as transforms
import numpy as np
import torch
from keras import backend as K
import sys

opt = TestOptions().parse()
opt.nThreads = 1   # test code only supports nThreads = 1
opt.batchSize = 1  # test code only supports batchSize = 1
opt.serial_batches = True  # no shuffle
opt.no_flip = True  # no flip

data_loader = CreateDataLoader(opt)
dataset = data_loader.load_data()
model = create_model(opt)
visualizer = Visualizer(opt)
test_transform = transforms.Compose([
    transforms.Scale((256,256)),  # resized to the network's required input size
    transforms.ToTensor(),])
def preprocess_input(x, data_format=None, version=1):
    x_temp = np.copy(x)
    if data_format is None:
        data_format = K.image_data_format()
    assert data_format in {'channels_last', 'channels_first'}

    if version == 1:
        if data_format == 'channels_first':
            x_temp = x_temp[:, ::-1, ...]
            x_temp[:, 0, :, :] -= 93.5940
            x_temp[:, 1, :, :] -= 104.7624
            x_temp[:, 2, :, :] -= 129.1863
        else:
            x_temp = x_temp[..., ::-1]
            x_temp[..., 0] -= 93.5940
            x_temp[..., 1] -= 104.7624
            x_temp[..., 2] -= 129.1863

    elif version == 2:
        if data_format == 'channels_first':
            x_temp = x_temp[:, ::-1, ...]
            x_temp[:, 0, :, :] -= 91.4953
            x_temp[:, 1, :, :] -= 103.8827
            x_temp[:, 2, :, :] -= 131.0912
        else:
            x_temp = x_temp[..., ::-1]
            x_temp[..., 0] -= 91.4953
            x_temp[..., 1] -= 103.8827
            x_temp[..., 2] -= 131.0912
    else:
        raise NotImplementedError

    return x_temp
is_save_img = 0
if is_save_img == 1:
    # create website
    web_dir = os.path.join(opt.results_dir, opt.name, '%s_%s' % (opt.phase, opt.which_epoch))
    webpage = html.HTML(web_dir, 'Experiment = %s, Phase = %s, Epoch = %s' % (opt.name, opt.phase, opt.which_epoch))
    # test
    for i, data in enumerate(dataset):
        if i >= opt.how_many:
            break
        print (data)
        model.set_input(data)
        model.test()
        visuals = model.get_current_visuals()
        img_path = model.get_image_paths()
        print('%04d: process image... %s' % (i, img_path))
        visualizer.save_images(webpage, visuals, img_path)

    webpage.save()
if is_save_img == 0:
    img_path_vidA = '../testVidJ2O/Kout_1_1.avi'
    img_path_vidB = '../testVidJ2O/Out_Obama2_1_1.avi'
    vidA = cv2.VideoCapture(img_path_vidA)
    vidB = cv2.VideoCapture(img_path_vidB)

    img_size = 256
    fps = 15
    outVid = cv2.VideoWriter('AtoB1_19.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), fps, (2 * img_size, img_size))
    outVid1 = cv2.VideoWriter('BtoA1_19.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), fps, (2 * img_size, img_size))
    while True:
        retA, imgA_src = vidA.read()
        retB, imgB_src = vidB.read()

        if retA is False or retB is False:
            print(' frame is empty')
            sys.exit()
            continue
        # model.set_input(imgA)
        imgA_src = cv2.resize(imgA_src, (img_size, img_size))
        imgB_src = cv2.resize(imgB_src, (img_size, img_size))
        imgA = cv2.cvtColor(imgA_src, cv2.COLOR_BGR2RGB)
        imgB = cv2.cvtColor(imgB_src, cv2.COLOR_BGR2RGB)

        imgA = np.asarray(imgA, dtype=np.float32)
        # imgA = preprocess_input(imgA, version=2)
        imgA = imgA/255.
        input_A = np.transpose(imgA, (2, 0, 1))
        input_A = np.expand_dims(input_A, 0).copy()
        input_A = torch.from_numpy(input_A)

        imgB = np.asarray(imgB, dtype=np.float32)
        # imgB = preprocess_input(imgB, version= 2)
        imgB = imgB/255.
        input_B = np.transpose(imgB, (2, 0, 1))
        input_B = np.expand_dims(input_B, 0).copy()
        input_B = torch.from_numpy(input_B)


        fakeB, fakeA = model.testVid(input_A, input_B)

        # Combined image
        img_combined = np.concatenate((imgA_src, fakeB), axis=1)
        img_combined1 = np.concatenate((imgB_src, fakeA), axis=1)

        outVid.write(img_combined)
        outVid1.write(img_combined1)

        cv2.imshow('display', img_combined)
        cv2.imshow('display1', img_combined1)
        cv2.waitKey(1)
    outVid.release()
    outVid1.release()